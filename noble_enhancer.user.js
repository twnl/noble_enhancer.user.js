// ==UserScript==
// @name        Noble Enhancer
// @version     1.0
// @author      Unfriendly Sander
// @grant       none
// @include     *.tribalwars.nl*screen=overview_villages*
// ==/UserScript==

var nobleEnhancer = ( function ( $ ) {
    'use strict';
    var villageNameIndex = 1;
    var noblesAvailable = null;
    var villages = [];
    var nobleRequirements = {
        population: 100,
        wood: 40000,
        stone: 50000,
        iron: 50000
    };

    var VillageInformation = function ( coord, $elem, link ) {
        if ( !( this instanceof VillageInformation ) ) {
            return new VillageInformation( coord, $elem, link );
        }

        this.coord = coord;
        this.elem = $elem;
        this.link = link;
        this.home = parseInt( $elem.find( 'a' ).text().match( /\d+/ ).pop(), 10 );
        this.production = 0;
        this.available = 0;
        this.away = 0;
    };

    VillageInformation.prototype.update = function () {
        this.elem.html( this.toString() );
    };

    VillageInformation.prototype.toString = function () {
        var str = ( this.available ) ? '<a href="' + this.link + '">' + this.home + '</a>' : this.home;

        str += ' / ' + ( this.away + this.home ) + ' + ' + this.production + ' (<span class="grey">' + this.available + '</span>)';

        return str;
    };

    VillageInformation.prototype.increment = function ( prop, amount ) {
        this[ prop ] += amount;
        return this;
    };

    var changeLinks = function () {
        var $rows = $( '#combined_table' ).find( 'td' ).closest( 'tr' ).not( '#filter' );
        var i = 0;
        var l = $rows.length;
        var $a;
        var link;

        for ( i; i < l; i++ ) {
            $a = $rows.eq( i ).find( 'a[href*="screen=snob"]' );
            link = $a.attr( 'href' ) + '&action=train&h=' + game_data.csrf;

            $a.attr( 'href', link );
        }
    };

    var updateCombined = function () {
        villages.forEach( function ( village ) {
            village.update();
        } );

        UI.SuccessMessage( 'Edels ingelezen' );
    };

    var addNobleFooter = function () {
        $('#footer').find('#linkContainer').append(' - Er kan nog geproduceerd worden: <span id="noble-enhancer-count">' + noblesAvailable + '</span>');
    }

    var initVillages = function () {
        var $rows = $( '#combined_table' ).find( 'td' ).closest( 'tr' ).not( '#filter' );
        var $row;
        var $a;
        var coord;

        for ( var i = 0, l = $rows.length; i < l; i++ ) {
            $row = $rows.eq( i );
            $a = $row.find( 'a' ).filter( '[href*="action=train"]' );
            coord = $row.find( 'td' ).eq( villageNameIndex ).find( 'a' ).first().text().match( /\d{1,3}\|\d{1,3}/ ).pop();
            villages.push( VillageInformation( coord, $a.parent(), $a.attr( 'href' ) ) );
        }

        getVillagesInformation();
    };

    var incrementAmount = function ( coord, prop, amount ) {
        villages.filter( function ( village ) {
            return village.coord === coord;
        } ).pop().increment( prop, amount ).update();
    };

    var getNobleCountIndex = function ( dom ) {
        var $ths = $( dom ).find( '#commands_table' ).find( 'tr' ).first().find( 'th' );

        for ( var i = 0, l = $ths.length; i < l; i++ ) {
            if ( $ths.eq( i ).find( 'img[src*="unit_snob"]' ).length ) {
                return i;
            }
        }

        return $ths.length - 1;
    };

    var getVillagesInformation = function () {
        $.when( getCommandScreen(), getProductionScreen(), getNobleScreen() )
            .done( function ( commands, production, nobles ) {
                noblesAvailable = parseInt( $( nobles[ 0 ] ).find( 'th:contains("geproduceerd worden")' ).parent().find( 'th' ).last().text().match( /\d+/ ).pop(), 10 );
                searchProductionScreen( production[ 0 ] );
                searchCommandsScreen( commands[ 0 ] );
                updateCombined();
                addNobleFooter();
            } );
    };

    var getNobleScreen = function () {
        return $.ajax( {
            url: game_data.link_base_pure + 'snob'
        } );
    };

    var getCommandScreen = function () {
        return $.ajax( {
            url: game_data.link_base_pure + 'overview_villages&mode=commands'
        } );
    };

    var getProductionScreen = function () {
        return $.ajax( {
            url: game_data.link_base_pure + 'overview_villages&mode=prod'
        } );
    };

    var inVillageCollection = function ( coord ) {
        return villages.filter( function ( village ) {
            return village.coord === coord;
        } ).length;
    };

    var searchCommandsScreen = function ( html ) {
        var $rows = $( html ).find( '#commands_table' ).find( 'td' ).closest( 'tr' );
        var i = 0;
        var l = $rows.length;
        var coord;
        var amount;
        var nobleCountIndex = getNobleCountIndex( html );

        for ( i; i < l; i++ ) {
            amount = parseInt( $rows.eq( i ).find( 'td' ).eq( nobleCountIndex ).text().match( /\d+/ ).pop(), 10 );
            coord = $rows.eq( i ).find( 'td' ).eq( 1 ).find( 'a' ).first().text().match( /\d{1,3}\|\d{1,3}/ ).pop();

            if ( amount ) {
                incrementAmount( coord, 'away', amount );
            }
        }
    };

    var getBoerderijIndex = function ( dom ) {
        var $ths = $( dom ).find( '#production_table' ).find( 'tr' ).first().find( 'th' );

        for ( var i = 0, l = $ths.length; i < l; i++ ) {
            if ( $ths.eq( i ).text().toLowerCase() === 'boerderij' ) {
                return i;
            }
        }

        return 6;
    };

    var calculateAvailableNobles = function ( nobleSpace, resources ) {
        var lowest = nobleSpace;

        for ( var prop in resources ) {
            if ( resources.hasOwnProperty( prop ) ) {
                resources[ prop ] = Math.floor( resources[ prop ] / nobleRequirements[ prop ] );

                if ( resources[ prop ] < lowest ) {
                    lowest = resources[ prop ];
                }
            }
        }
        return lowest;
    };

    var searchProductionScreen = function ( data ) {
        var $rows = $( data ).find( '#production_table' ).find( 'td' ).closest( 'tr' );
        var i = 0;
        var l = $rows.length;
        var boerderijIndex = getBoerderijIndex( data );
        var coord;
        var amount;
        var available;
        var farmSpace;
        var resources;

        for ( i; i < l; i++ ) {
            farmSpace = $rows.eq( i ).find( 'td' ).eq( boerderijIndex ).text().match( /\d+\/\d+/ ).pop().split( '/' );
            farmSpace = Math.floor( ( farmSpace[ 1 ] - farmSpace[ 0 ] ) / nobleRequirements.population );
            coord = $rows.eq( i ).find( 'td' ).eq( villageNameIndex ).find( 'a' ).text().match( /\d{1,3}\|\d{1,3}/ ).pop();
            amount = $rows.eq( i ).find( '.queue_icon' ).find( 'img' ).filter( '[src*="snob.png"]' ).length;
            resources = {
                wood: parseInt( $rows.eq( i ).find( '.wood' ).text().match( /\d+/g ).join( '' ), 10 ),
                stone: parseInt( $rows.eq( i ).find( '.stone' ).text().match( /\d+/g ).join( '' ), 10 ),
                iron: parseInt( $rows.eq( i ).find( '.iron' ).text().match( /\d+/g ).join( '' ), 10 )
            };

            available = calculateAvailableNobles( farmSpace, resources );

            if ( amount ) {
                incrementAmount( coord, 'production', amount );
            }

            if ( available ) {
                incrementAmount( coord, 'available', available );
            }
        }
    };

    var init = function () {
        $( '#overview_menu' ).append( '<tr><td><a href="#" id="nobles-read" style="text-align:center">Lees edels</a></td></tr>' );

        if ( !$( '.note-icon' ).length ) {
            villageNameIndex = 0;
        }

        changeLinks();
        bindEventHandlers();
    };

    var noMoreNobles = function () {
        $('#noble-enhancer-count').text(0);
        villages.forEach( function ( village ) {
            village.available = 0;
            village.update();
        } );
    };

    var bindEventHandlers = function () {
        $( '#nobles-read' ).on( 'click', function ( e ) {
            e.preventDefault();

            initVillages();
        } );

        $( '#combined_table' ).find( 'td' ).closest( 'tr' ).not( '#filter' ).on( 'click', 'a[href*="action=train"]', function ( e ) {
            e.preventDefault();

            var coord = $( this ).closest( 'tr' ).find( 'td' ).eq( villageNameIndex ).find( 'a' ).first().text().match( /\d{1,3}\|\d{1,3}/ ).pop();
            var link = $( this ).attr( 'href' );

            $.ajax( {
                url: link
            } ).done( function () {
                incrementAmount( coord, 'production', 1 );
                incrementAmount( coord, 'available', -1 );

                noblesAvailable--;

                if ( noblesAvailable <= 0 ) {
                    noMoreNobles();
                } else {
                    $('#noble-enhancer-count').text(noblesAvailable);
                }
            } ).fail( function () {
                console.log( 'noble recruut failed silently...' );
            } );
        } );
    };

    return {
        init: init
    };
}( window.jQuery ) );

$( function () {
    if ( $( '#combined_table' ).length ) {
        nobleEnhancer.init();
    }
} );
